from django.db import models

from django.utils.timezone import now
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin


class AllCountries(models.Model):
    sortName = models.CharField(max_length=255)
    countryName = models.CharField(max_length=255)
    phoneCode = models.IntegerField()
    flagIconUrl = models.CharField(max_length=255, null=True, default=None)

    def __int__(self):
        return self.flagIconUrl

    class Meta:
        db_table = 'all_countries'


class deviceType(models.Model):
    name = models.CharField(max_length=255, null=False)
    createdAt = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'deviceTypes'


class CustomAccountManager(BaseUserManager):
    def create_superuser(self, email, fullName, password, **other_fields):
        # other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)

        return self.create_user(email, fullName, password, **other_fields)


class User(AbstractBaseUser, PermissionsMixin):
    providers = ((1, "google"), (2, "facebook"), (3, "apple"))

    countryCodeId = models.ForeignKey(AllCountries, on_delete=models.CASCADE,
                                         related_name='countryCode_ref', db_column='countryCodeId', null=True, default=None)
    firstName = models.CharField(max_length=255, null=False)
    lastName = models.CharField(max_length=255, null=True)
    mobileNo = models.CharField(max_length=255, unique=True, null=True, default=None)
    email = models.EmailField(max_length=255, unique=True, null=True, default=None)
    password = models.CharField(max_length=255, null=False)
    pin = models.CharField(max_length=255, null=True, default=None)
    otp=models.PositiveIntegerField(default=None, null=True)
    isActive = models.BooleanField(default=True)
    isDeleted = models.BooleanField(default=False)
    isVerified = models.BooleanField(default=False)
    deviceTypeId = models.ForeignKey(deviceType, on_delete=models.CASCADE,
                                     related_name='deviceType_ref', db_column='deviceTypeId', null=True, default=None)

    deviceToken = models.CharField(max_length=255, null=True)
    profileImage = models.CharField(max_length=255, null=True, blank=True)
    last_login = models.DateTimeField(default=now, editable=False)
    admin_forget_password_token = models.CharField(
        max_length=200, default=False, null=True)

    authServiceProviderId = models.CharField(
        max_length=255, null=True, unique=True)
        
    authServiceProviderType = models.IntegerField(choices=providers, null=True)
    fromCountryId = models.ForeignKey(AllCountries, on_delete=models.CASCADE,
                                         related_name='from_countryCode_ref', db_column='fromCountryId', null=True, default=None)

    createdAt = models.DateTimeField(default=now, editable=False)
    updatedAt = models.DateTimeField(default=now, editable=False)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['firstName', 'mobileNo']
    objects = CustomAccountManager()

    class Meta:
        db_table = 'users'

# class FromCountry(models.Model):
#     userId = models.ForeignKey(User, on_delete=models.CASCADE,
#                                      related_name='fromCountryUserId_ref', db_column='userId', null=False)
#     countryId = models.ForeignKey(AllCountries, on_delete=models.CASCADE,
#                                          related_name='fromCountry_ref', db_column='countryId', null=False, default=None)
#     createdAt = models.DateTimeField(default=now, editable=False)
#     class Meta:
#         db_table = 'from_countries'

class ToCountry(models.Model):
    userId = models.ForeignKey(User, on_delete=models.CASCADE,
                                     related_name='toCountryUserId_ref', db_column='userId', null=False)
    countryId = models.ForeignKey(AllCountries, on_delete=models.CASCADE,
                                         related_name='toCountry_ref', db_column='countryId', null=False, default=None)
    createdAt = models.DateTimeField(default=now, editable=False)
    class Meta:
        db_table = 'to_countries'


# class ResetPassword(models.Model):
#     user=models.ForeignKey(User,related_name='reset_passwords',on_delete=models.CASCADE)
#     otp=models.PositiveIntegerField()
#     status=models.CharField(max_length=255,default='not_verified')
#     createdAt = models.DateTimeField(default=now, editable=False)
    
    

  
class TemReg(models.Model):
    email = models.EmailField(max_length=255, null=True, default=None)
    otp=models.PositiveIntegerField()
    createdAt = models.DateTimeField(default=now, editable=False)
    

