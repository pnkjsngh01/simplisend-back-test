from Helpers.helper import send_email, gen_otp
from auth_APIs.models import *
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.parsers import JSONParser
import io
from rest_framework import status
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken
from auth_APIs.models import *
from auth_APIs.serializer import AllCountryCodeSerializer, CustomerRegistrationSerializer
from django.contrib.auth.hashers import make_password
from rest_framework.decorators import api_view
from django.db.models import Q
from rest_framework.views import APIView


# Create your views heclass CountryCodeList(ListAPIView):

class CountryCodeList(ListAPIView):
    permission_classes = [AllowAny, ]

    def list(self, request):
        top5Countries = AllCountries.objects.filter(countryName__in=[
                                                    "Nigeria", "Ghana", "Senegal", "Tanzania", "South Africa", "Jamaica", "Philippines", "India", "Mexico"]).all().order_by("countryName")
        serializer = AllCountryCodeSerializer(top5Countries, many=True)
        remaining = AllCountryCodeSerializer(AllCountries.objects.all().exclude(countryName__in=[
                                             "Nigeria", "Ghana", "Senegal", "Tanzania", "South Africa", "Jamaica", "Philippines", "India", "Mexico"]).order_by("countryName"), many=True)
        response = {
            "error": None,
            "response": {
                "top5countries": serializer.data,
                "data": remaining.data,
                "message": {
                    'success': True,
                    "successCode": 101,
                    "statusCode": status.HTTP_200_OK,
                    "successMessage": "All country code data."
                }
            }
        }
        return Response(response, status=status.HTTP_200_OK)


class CustomerRegistrationView(CreateAPIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            pythonData = JSONParser().parse(io.BytesIO(request.body))
            deviceTypeId = pythonData.get('deviceTypeId', False)
            mobileNo = pythonData.get('mobileNo', False)
            password = pythonData.get('password', False)

            countryCodeId = pythonData.get('countryCodeId', False)
            fromCountryId = pythonData.get('fromCountryId', False)
            toCountryIds = pythonData.get('toCountryIds', False)
            pin = pythonData.get('pin', False)
            email = pythonData.get('email', False)

            if not email and not mobileNo:
                response = {
                    "error": {
                        "errorCode": 500,
                        "statusCode": status.HTTP_422_UNPROCESSABLE_ENTITY,
                        "errorMessage": "email or mobile no missing"
                    },
                    "response": None
                }
                return Response(response, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            if not deviceTypeId or not password:
                response = {
                    "error": {
                        "errorCode": 501,
                        "statusCode": status.HTTP_422_UNPROCESSABLE_ENTITY,
                        "errorMessage": "required parameter missing please check"
                    },
                    "response": None
                }
                return Response(response, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            if mobileNo:
                if not countryCodeId:
                    response = {
                        "error": {
                            "errorCode": 501,
                            "statusCode": status.HTTP_422_UNPROCESSABLE_ENTITY,
                            "errorMessage": "countryCodeId is required"
                        },
                        "response": None
                    }
                    return Response(response, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            if not toCountryIds or not fromCountryId:
                response = {
                    "error": {
                        "errorCode": 504,
                        "statusCode": status.HTTP_404_NOT_FOUND,
                        "errorMessage": "fromCountry and toCountry field required"
                    },
                    "response": None
                }
                return Response(response, status=status.HTTP_404_NOT_FOUND)

            userCheck = User.objects.filter(
                Q(Q(mobileNo=mobileNo) | Q(email=email)) & Q(isDeleted=False)).first()
            if userCheck:
                response = {
                    "error": {
                        "errorCode": 501,
                        "statusCode": status.HTTP_422_UNPROCESSABLE_ENTITY,
                        "errorMessage": "User already registered"
                    },
                    "response": None
                }
                return Response(response, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            pythonData["password"] = make_password(password)
            if pin:
                pythonData["pin"] = make_password(pin)
            pythonData["fromCountryId"] = fromCountryId

            serializerCall = CustomerRegistrationSerializer(data=pythonData)
            if serializerCall.is_valid(raise_exception=True):
                user = serializerCall.save()
                for toId in toCountryIds:
                    ToCountry.objects.create(
                        userId=user, countryId=AllCountries.objects.filter(id=toId).first())
                refresh = RefreshToken.for_user(user)

                data = {
                    "userId": user.id,
                    "mobileNo": user.mobileNo,
                    "token": str(RefreshToken.for_user(user).access_token),
                    "refreshToken": str(refresh),
                }
                response = {
                    "error": None,
                    "response": {
                        "userData": data,
                        "message": {
                            'success': True,
                            "successCode": 101,
                            "statusCode": status.HTTP_200_OK,
                            "successMessage": "Customer registered successfully."
                        }
                    }
                }
                return Response(response, status=status.HTTP_200_OK)
            else:
                response = {
                    "error": {
                        "errorCode": 509,
                        "statusCode": status.HTTP_500_INTERNAL_SERVER_ERROR,
                        "errorMessage": "Error while registring user. Please try again later."
                    },
                    "response": None
                }
                return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        except Exception as exception:
            response = {
                "error": {
                    "errorCode": 504,
                    "statusCode": status.HTTP_400_BAD_REQUEST,
                    "errorMessage": str(exception)
                },
                "response": None
            }
            return Response(response, status=status.HTTP_400_BAD_REQUEST)


@api_view(['post'])
def changePassword(request):
    try:
        email = request.data.get('email', '')
        mobileNo = request.data.get('mobileNo', '')
        newpassword = request.data.get('newpassword', '')

        userInput = None
        if email:
            userInput = email
        elif mobileNo:
            userInput = mobileNo
        else:
            return Response({
                'error': {
                    "errorCode": 112,
                    "statusCode": status.HTTP_401_UNAUTHORIZED,
                    "errorMessage": "Please provide email or mobile no",
                },
                'response': None
            })

        if not newpassword:
            return Response({
                'error': {
                    "errorCode": 111,
                    "statusCode": status.HTTP_401_UNAUTHORIZED,
                    "errorMessage": "Please provide new password",
                },
                'response': None
            })

        if not User.objects.filter(Q(email=userInput) | Q(mobileNo=userInput)).first():

            return Response({
                'error': {
                    "errorMessage": "User Not Found",
                    "errorCode": 108,
                    "statusCode": 401
                },
                'response': None
            })
        instance = User.objects.get(Q(email=email) | Q(mobileNo=mobileNo))
        instance.set_password(newpassword)
        instance.save()

        response = {
            "error": None,
            "response": {
                "message": {
                    'success': True,
                    "successCode": 101,
                    "statusCode": status.HTTP_200_OK,
                    "successMessage": "Update password successfully."
                }
            }
        }
        return Response(response, status=status.HTTP_200_OK)

    except Exception as exception:
        response = {
            "error": {
                "errorCode": 504,
                "statusCode": status.HTTP_400_BAD_REQUEST,
                "errorMessage": str(exception)
            },
            "response": None
        }
    return Response(response, status=status.HTTP_400_BAD_REQUEST)


class AccountExistenceCheck(RetrieveAPIView):

    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            pythonData = JSONParser().parse(io.BytesIO(request.body))
            email = pythonData.get('email', False)
            mobileNo = pythonData.get('mobileNo', False)
            if not email and not mobileNo:
                response = {
                    "error": {
                        "errorCode": 500,
                        "statusCode": status.HTTP_422_UNPROCESSABLE_ENTITY,
                        "errorMessage": "Pass atleast one parameter"
                    },
                    "response": None
                }
                return Response(response, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            userCheck = User.objects.filter(
                Q(email=email) & Q(isDeleted=False) | Q(mobileNo=mobileNo)).first()

            response = {
                "error": None,
                "response": {
                    "isExists": True if userCheck else False,
                    "countryCode": (userCheck.countryCodeId.phoneCode if userCheck.mobileNo else None) if userCheck else None,
                    "message": {
                        'success': True,
                        "successCode": 101,
                        "statusCode": status.HTTP_200_OK,
                        "successMessage": "Email already exist"
                    }
                }
            }
            return Response(response, status=status.HTTP_200_OK)
        except Exception as e:
            status_code = status.HTTP_400_BAD_REQUEST
            response = {
                "error": {
                    "errorCode": 616,
                    "statusCode": status.HTTP_400_BAD_REQUEST,
                    "errorMessage": str(e)
                },
                "response": None
            }
            return Response(response, status=status_code)


@api_view(['post'])
def Login(request):

    try:

        data = request.data

        # mobileNo = data.get('mobileNo',False)
        email = data.get('email', False)
        password = data.get('password', False)
        mobileNo = data.get('mobileNo', False)

        userr = None

        if email:
            userr = User.objects.filter(email=email)
        if mobileNo:
            userr = User.objects.filter(mobileNo=mobileNo)

        if userr.exists() and userr[0].check_password(password):
            userr = userr[0]

            refresh = RefreshToken.for_user(userr)

            return Response({

                "error": None,
                "response": {
                            "data": {
                                'token': str(refresh.access_token),
                                'refreshToken': str(refresh),
                            },
                            "message": {
                                "success": True,
                                "successCode": 203,
                                "statusCode": status.HTTP_200_OK,
                                "successMessage": "User logged in successfully."
                            }
                            }
            })

        else:
            return Response({
                'error': {
                    "statusCode": status.HTTP_401_UNAUTHORIZED,
                    "errorCode": 400,
                    "errorMessage": "Please enter your valid credentials"
                },
                'response': None
            })

        # if True:

        #     userr = User.objects.get(mobileNo=mobileNo)

        # tokens = get_tokens_for_user(userr)

        # return Response({

        #             "error": None,
        #             "response": {
        #                 "data": tokens,
        #                 "message": {
        #                     "success": True,
        #                     "successCode": 203,
        #                     "statusCode": status.HTTP_200_OK,
        #                     "successMessage": "Please check OTP on your email"
        #             }
        #         }
        #     })

    except Exception as e:
        return Response({
            'error': {
                        "statusCode": status.HTTP_400_BAD_REQUEST,
                        "errorCode": 400,
                        "errorMessage": "Error: "+str(e)
                        },
            'response': None
        })


@api_view(['post'])
def SocialLogin(request):

    try:

        data = request.data

        # mobileNo = data.get('mobileNo',False)
        email = data.get('email', False)

        authServiceProviderId = data.get('authServiceProviderId', False)
        authServiceProviderType = data.get('authServiceProviderType', False)
        # password = data.get('password',False)

        userr = None

        if authServiceProviderId and authServiceProviderType and email:
            tempUser = User.objects.filter(
                Q(email=email),
                Q(authServiceProviderType=authServiceProviderType),
                Q(authServiceProviderId=authServiceProviderId),
                isActive=True
            )

            if tempUser.exists():
                userr = tempUser[0]

                refresh = RefreshToken.for_user(userr)

                return Response({

                    "error": None,
                    "response": {
                                "data": {
                                    'token': str(refresh.access_token),
                                    'refreshToken': str(refresh)
                                },
                                "message": {
                                    "success": True,
                                    "successCode": 203,
                                    "statusCode": status.HTTP_200_OK,
                                    "successMessage": "User logged in successfully."
                                }
                                }
                })

            else:

                testt = User.objects.filter(
                    Q(email=email),
                )

                if testt.exists():

                    if testt[0].isActive:

                        return Response({
                            'error': {
                                "statusCode": status.HTTP_400_BAD_REQUEST,
                                "errorCode": 400,
                                "errorMessage": "User already registered ."
                            },
                            'response': None
                        })

                    if not testt[0].isActive:

                        return Response({
                            'error': {
                                "statusCode": status.HTTP_302_FOUND,
                                "errorCode": 302,
                                "errorMessage": "Please provide other required data too."
                            },
                            'response': None
                        })

                else:

                    userr = User.objects.create(
                        email=email,
                        authServiceProviderId=authServiceProviderId,
                        authServiceProviderType=authServiceProviderType,
                        isActive=False

                    )

                    return Response({
                        'error': {
                            "statusCode": status.HTTP_201_CREATED,
                            "errorCode": 201,
                            "errorMessage": "Please provide other required data."
                        },
                        'response': None
                    })

        else:

            return Response({
                'error': {
                    "statusCode": status.HTTP_401_UNAUTHORIZED,
                    "errorCode": 401,
                    "errorMessage": "Please provide valid data."
                },
                'response': None
            })

    except Exception as e:
        return Response({
            'error': {
                "statusCode": status.HTTP_401_UNAUTHORIZED,
                "errorCode": 400,
                "errorMessage": "Error "+str(e)
            },
            'response': None
        })


@api_view(['post'])
def SocialLogin2(request):

    try:

        data = request.data

        testUser = User.objects.filter(email=data.get('email', ''))

        authServiceProviderId = None
        authServiceProviderType = None

        if testUser.exists():
            authServiceProviderId = testUser[0].authServiceProviderId
            authServiceProviderType = testUser[0].authServiceProviderType
            testUser.delete()

        serializerCall = CustomerRegistrationSerializer(data=data)
        if serializerCall.is_valid():
            user = serializerCall.save()

            userr = User.objects.get(email=data.get('email', ''))
            userr.authServiceProviderId = authServiceProviderId
            userr.authServiceProviderType = authServiceProviderType
            userr.isActive = True
            userr.save()

            for toId in data.get('toCountryIds', False):
                ToCountry.objects.create(
                    userId=user, countryId=AllCountries.objects.filter(id=toId).first())

            refresh = RefreshToken.for_user(user)
            data = {
                "userId": user.id,
                "mobileNo": user.mobileNo,
                "token": str(RefreshToken.for_user(user).access_token),
                "refreshToken": str(refresh),
            }
            response = {
                "error": None,
                "response": {
                    "userData": data,
                    "message": {
                        'success': True,
                        "successCode": 101,
                        "statusCode": status.HTTP_200_OK,
                        "successMessage": "Customer registered successfully."
                    }
                }
            }
            return Response(response, status=status.HTTP_200_OK)
        else:
            response = {
                "error": {
                    "errorCode": 509,
                    "statusCode": status.HTTP_500_INTERNAL_SERVER_ERROR,
                    "errorMessage": "Error while registring user. Please try again later."
                },
                "response": None
            }
            return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    except Exception as e:
        response = {
            "error": {
                "errorCode": 509,
                "statusCode": status.HTTP_500_INTERNAL_SERVER_ERROR,
                "errorMessage": "Error while registring user."+str(e)
            },
            "response": None
        }
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['post'])
def forgotpass(request):
    try:
        email = request.data.get('email')

        if not email:
            return Response({
                'error': {
                    "errorCode": 111,
                    "statusCode": status.HTTP_401_UNAUTHORIZED,
                    "errorMessage": "Please provide email",
                },
                'response': None
            })

        if not User.objects.filter(Q(email=email)).first():

            return Response({
                'error': {
                    "errorCode": 511,
                    "statusCode": status.HTTP_400_BAD_REQUEST,
                    "errorMessage": 'User Not Found'
                },
                'response': None
            })

        user_obj = User.objects.get(email=email)
        otp = gen_otp()

        send_email(user_obj.email, 'Simplisend PasswordReset otp',
                   f'Forgot password OTP is {otp}', fail_silent=True)

        user_obj.otp = otp
        user_obj.save()

        return Response({
            'error': None,
            'response': {
                'data': None,
                'message': {
                    "success": True,
                    "successCode": 200,
                    "statusCode": status.HTTP_200_OK,
                    'successMessage': 'Please check your email for forgot/password reset otp'
                }
            }
        })
    except Exception as e:
        return Response({
            'error': {
                "errorMessage": str(e),
                "errorCode": status.HTTP_204_NO_CONTENT,
                "statusCode": 204,
            },
            'response': None

        })


@api_view(['post'])
def verifyotp(request):
    try:
        email = request.data.get('email', '')
        otp = request.data.get('otp', '')

        if not email or not otp:
            return Response({
                'error': {
                    "errorCode": 105,
                    "statusCode": status.HTTP_400_BAD_REQUEST,
                    "errorMessage": "Email or otp not provided"
                },
                'response': None
            })
        if not User.objects.filter(Q(email=email)).first():
            # messages.warning(request, '')
            return Response({
                'error': {
                    "errorCode": 110,
                    "statusCode": status.HTTP_400_BAD_REQUEST,
                    "errorMessage": "Invalid otp",
                },
                'response': None
            })

        resetuser_obj = User.objects.get(email=email)
        # print(resetuser_obj)

        if resetuser_obj.otp != otp:
            return Response({

                'error': {
                    "errorCode": 107,
                    "statusCode": "",
                    'errorMessage': 'Invalid otp'
                },
                'response': None
            })

        if resetuser_obj.otp == otp:

            # resetuser_obj.delete()

            return Response({
                "error": None,
                'response': {
                    "data": None,
                    "message": {
                        "success": True,
                        "successCode": 200,
                        "statusCode": status.HTTP_200_OK,
                        "successMessage": "OTP verified successfully."
                    }
                }
            })

    except Exception as e:
        return Response({
            'error': {
                        "statusCode": status.HTTP_400_BAD_REQUEST,
                        "errorCode": 400,
                        "errorMessage": "Error: "+str(e)
                        },
            'response': None
        })


class Upload(ListAPIView):
    permission_classes = [AllowAny, ]


# import glob
# class Upload(ListAPIView):
#     permission_classes = [AllowAny, ]

#     def get(self, request):

#         allCountry = AllCountries.objects.filter(flagIconUrl=None).exclude(sortName__in=['TP','XA','XU','XJ','XM','AN','XG','YU']).values_list('sortName', flat=True)
#         print(allCountry)
#         # import urllib.request

#         for filename in glob.glob('/home/rizwan/Apponward/simplisend-admin-api/Icons/*.png'):
#             print(filename.split('/')[-1])
#             nm = filename.split('/')[-1]
#             url = s3_helper(nm)
#             print(url)
#             # with open(filename , 'rb') as f:
#             #     url =  s3_helper(f)
#             #     print(url)

#         # for country in allCountry:

#             # serializer =  requests.get(f"https://restcountries.com/v3.1/alpha/{country}")
#             # res = serializer.json()
#             # print(res[0]["name"]["common"])
#             # print(res[0]["flags"]["png"])
#             # image_name = res[0]["name"]["common"]+".png"
#             # res = requests.get(res[0]["flags"]["png"], stream = True)
#             # if res.status_code == 200:
#             #     with open(image_name,'wb') as f:
#             #         shutil.copyfileobj(res.raw, f)
#             #     print('Image sucessfully Downloaded: ',image_name)
#             # else:
#             #     print('Image Couldn\'t be retrieved')
#             # url = s3_helper(res[0]["flags"]["png"])
#             # print(url)
#             # county = AllCountries.objects.filter(sortName__exact = country).update(flagIconUrl=url)
#         # serializer =  requests.get(f"https://restcountries.com/v3.1/alpha/EC")
#         # res = serializer.json()
#         # print(res[0]["name"]["common"])
#         # print(res[0]["flags"]["png"])
#         # image_name = res[0]["name"]["common"]+".png"
#         # res = requests.get(res[0]["flags"]["png"], stream = True)
#         # if res.status_code == 200:
#         #     with open(image_name,'wb') as f:
#         #         shutil.copyfileobj(res.raw, f)
#         #     print('Image sucessfully Downloaded: ',image_name)
#         # else:
#         #     print('Image Couldn\'t be retrieved')
#         # urp = send_image_to_s3(res[0]["flags"]["png"])
#         # print(urp)

#         response = {
#             "error": None,
#             "response": {
#                 "message": {
#                     'success': True,
#                     "successCode": 101,
#                     "statusCode": status.HTTP_200_OK,
#                     "successMessage": "All country code data."
#                 }
#             }
#         }
#         return Response(response, status=status.HTTP_200_OK)


class TempRegAPI(APIView):
    def post(self, request):
        try:

            email = request.data.get('email', '')

            if not email:
                return Response({
                    'error': {
                        "errorCode": 111,
                        "statusCode": status.HTTP_401_UNAUTHORIZED,
                        "errorMessage": "Please provide email",
                    },
                    'response': None
                })

            user = User.objects.filter(Q(email=email))

            if not user.exists():

                otp = gen_otp()

                user = TemReg.objects.filter(email=email)
                if user.exists():
                    user.update(otp=otp)
                    user = user[0]
                else:
                    user = TemReg.objects.create(email=email, otp=otp)

                send_email(user.email, 'Simplisend PasswordReset otp',
                           f'Email Verify  OTP is {otp}', fail_silent=True)

                return Response({
                    'error': None,
                    'response': {
                        'data': None,
                        'message': {
                            "success": True,
                            "successCode": 200,
                            "statusCode": status.HTTP_200_OK,
                            'successMessage': 'Please check your email for verify otp'
                        }
                    }
                })

            return Response({
                'error': {
                    "errorCode": 111,
                    "statusCode": status.HTTP_401_UNAUTHORIZED,
                    "errorMessage": "User already exist",
                },
                'response': None
            })

        except Exception as e:
            return Response({
                'error': {
                    "errorMessage": str(e),
                    'errorCode': 500,
                    'statusCode': 506
                },
                'response': None
            })


class emailverifyotp(APIView):
    def post(self, request):
        try:
            email = request.data.get('email', '')
            otp = request.data.get('otp', '')

            if not email or not otp:
                return Response({
                    'error': {
                        "errorCode": 105,
                        "statusCode": status.HTTP_400_BAD_REQUEST,
                        "errorMessage": "Email or otp not provided"
                    },
                    'response': None
                })
            if not TemReg.objects.filter(Q(email=email)).first():
                # messages.warning(request, '')
                return Response({
                    'error': {
                        "errorCode": 110,
                        "statusCode": status.HTTP_400_BAD_REQUEST,
                        "errorMessage": "Invalid email",
                    },
                    'response': None
                })

            resetuser_obj = TemReg.objects.get(email=email)
            # print(resetuser_obj)

            if resetuser_obj.otp != otp:
                return Response({

                    'error': {
                        "errorCode": 107,
                        "statusCode": "",
                        'errorMessage': 'Invalid otp'
                    },
                    'response': None
                })

            if resetuser_obj.otp == otp:
                # resetuser_obj.is_verified=True
                # resetuser_obj.save()

                resetuser_obj.delete()

                return Response({
                    "error": None,
                    'response': {
                        "data": None,
                        "message": {
                            "success": True,
                            "successCode": 200,
                            "statusCode": status.HTTP_200_OK,
                            "successMessage": "OTP verified successfully."
                        }
                    }
                })

        except Exception as e:
            return Response({
                'error': {
                    "errorMessage": str(e),
                    'errorCode': 500,
                    'statusCode': 506
                },
                'response': None
            })
