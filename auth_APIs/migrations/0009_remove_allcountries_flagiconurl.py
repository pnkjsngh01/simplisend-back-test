# Generated by Django 4.1.5 on 2023-01-18 10:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auth_APIs', '0008_user_fromcountryid_delete_fromcountry'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='allcountries',
            name='flagIconUrl',
        ),
    ]
