# Generated by Django 4.1.5 on 2023-01-09 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth_APIs', '0003_remove_allcountries_flagiconurl'),
    ]

    operations = [
        migrations.AddField(
            model_name='allcountries',
            name='flagIconUrl',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
    ]
