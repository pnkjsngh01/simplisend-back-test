from rest_framework.serializers import ModelSerializer
from auth_APIs.models import *
from rest_framework import serializers

class AllCountryCodeSerializer(ModelSerializer):
    class Meta:
        model = AllCountries
        fields = ['id', 'sortName', 'countryName', 'phoneCode','flagIconUrl']


class CustomerRegistrationSerializer(ModelSerializer):

    class Meta:
        model = User
        
        fields = ['id', 'firstName','lastName', 'password', 
                  'mobileNo', 'deviceTypeId','countryCodeId','pin','fromCountryId','email']
    def validate(self, attrs):
        return super().validate(attrs)
        
    def create(self, validated_data):
        return super().create(validated_data)



