from django.urls import path
from auth_APIs import views

urlpatterns = [
    path('country/code/list',views.CountryCodeList.as_view()),
    path('cust/registration', views.CustomerRegistrationView.as_view()),
    path('changePassword/', views.changePassword, name='changePassword'),
    path('account/existence/check', views.AccountExistenceCheck.as_view()),

    #... by madahv..
    path('login/',views.Login,name='login'),
    path('social_login/',views.SocialLogin,name='social_login'),
    path('social_login_step2/',views.SocialLogin2,name='social_login_step2'),

    path('image/upload', views.Upload.as_view()),
    path('changePassword', views.changePassword, name='changePassword'),
    path('forgotpass', views.forgotpass, name='forgotpass'),
    path('verifyotp', views.verifyotp, name='verifyotp'),
    path('tempreg', views.TempRegAPI.as_view()),
    path('emailverifyotp', views.emailverifyotp.as_view()),
    path('account/existence/check', views.AccountExistenceCheck.as_view())
]




