# from slay.settings import NOTIFICATION_SERVER_KEY
# import requests
import json
import boto3
import os
from simplisend_admin_api import settings
import uuid
from django.core.mail import send_mail , EmailMultiAlternatives, EmailMessage
import requests

from django.conf import settings



# def send_notification(title, message, deviceTokens, data):
#     serverToken = settings.NOTIFICATION_SERVER_KEY
#     headers = {
#         'Content-Type': 'application/json',
#         'Authorization': 'key=' + serverToken,
#     }


#     body = {
#         "registration_ids": deviceTokens,
#         "notification": {
#             "body": message,
#             "title": title,
#             "vibrate": 1,
#             "sound": 1
#         },
#         "data": data
#     }
#     response = requests.post(
#         "https://fcm.googleapis.com/fcm/send", headers=headers, data=json.dumps(body))
#     data = response.json()
#     if data['failure'] == 0:
#         return True
#     else:
#         return False

# def send_notification1(deviceTokens, data):
#     serverToken = NOTIFICATION_SERVER_KEY
#     headers = {
#         'Content-Type': 'application/json',
#         'Authorization': 'key=' + serverToken,
#     }

#     body = {
#         "registration_ids": deviceTokens,
#         "data": data
#     }
#     response = requests.post(
#         "https://fcm.googleapis.com/fcm/send", headers=headers, data=json.dumps(body))
#     data = response.json()
#     if data['failure'] == 0:
#         return True
#     else:
#         return False

def s3_helper(file):
    s3 = boto3.resource('s3', aws_access_key_id=settings.AWS_ACCESS_KEY,
                              aws_secret_access_key=settings.AWS_SECRET_KEY,
                              region_name=settings.REGION_NAME
                              )

    bucket = s3.Bucket(settings.S3_BUCKET)
    print(file)
    split_tup = os.path.splitext(file.name)
    file_extension = split_tup[1]
    new_file_name = "image"+str(uuid.uuid4())[:8]+file_extension
    bucket.put_object(Key=new_file_name, Body=file)
    file_url = 'https://simplisend-admin-bucket.s3.eu-west-2.amazonaws.com/'+new_file_name
    return file_url





def send_email(email, subject, message,fail_silent=False):
    subject = subject
    message = message
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [email]
    send_mail(subject, message, email_from, recipient_list,fail_silently=fail_silent)
   
    return True

import random
def gen_otp():
    otp = random.randint(100000,999999)
    return otp 
# def send_image_to_s3(url):
#     bucket_name = settings.S3_BUCKET
#     AWS_SECRET_ACCESS_KEY = settings.AWS_SECRET_KEY
#     AWS_ACCESS_KEY_ID = settings.AWS_ACCESS_KEY

    # s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID,
    #                   aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
   
    

    # file_name = f'https://simplisend-admin-bucket.s3.eu-west-2.amazonaws.com/{name}'
    # print('sending {}'.format(file_name))
    # r = s3.upload_fileobj(img)
    # s3 = boto3.resource('s3', aws_access_key_id=settings.AWS_ACCESS_KEY,
    #                           aws_secret_access_key=settings.AWS_SECRET_KEY,
    #                           region_name=settings.REGION_NAME
    #                           )

    # bucket = s3.Bucket(settings.S3_BUCKET)
    # obj = bucket.Object(AWS_ACCESS_KEY_ID)

    # # with open('filename', 'rb') as data:
    # r=obj.upload_fileobj(img)

    # print(r)

    # s3_path = 'https://simplisend-admin-bucket.s3.eu-west-2.amazonaws.com/' + img
    # return s3_path

