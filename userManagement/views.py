from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render, redirect
from auth_APIs.models import User
from django.contrib import messages
from django.views import View
from django.contrib.auth import login, logout
from django.contrib.auth.hashers import make_password, check_password
from simplisend_admin_api import settings
from django.core.mail import send_mail
import uuid
from django.db.models import Q
from django.contrib.auth.decorators import login_required

from Helpers.helper import s3_helper
from django.utils.timezone import now
import datetime


# Create your views here.


def admin_login(request):
    if request.method == "GET":
        return render(request, 'loginUser/login.html')

    else:
        email = request.POST.get('email', '')
        mobileNo = request.POST.get('email', '')
        password = request.POST.get('password', '')

        user = User.objects.filter(Q(email=email) | Q(mobileNo=mobileNo)).first()

        if user is None:
            messages.error(request, 'Incorrect Email OR Mobile No / Password')
            return redirect('admin-login')
        elif not user.check_password(password):
            messages.error(request, 'Incorrect email/password')
            return redirect('admin-login')

        else:
            if user.is_superuser == 1:
                login(request, user)
                return redirect('admin-dashboard')
                # return redirect('admin-dashboard')
            else:
                messages.error(request, 'Not authenticate user')
            return redirect('admin-login')


def send_email_forget_pass(email, token):
    subject = 'Your forget password link'
    message = f'Hi, click on the link and reset the password {settings.BASE_URL}/admin-change-password/{token}/'
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [email]
    send_mail(subject, message, email_from, recipient_list)
    return True


def admin_forget_password(request):
    if request.method == 'GET':
        return render(request, 'loginUser/forgot-password.html')
    else:
        email = request.POST.get('email')

        if not User.objects.filter(Q(email=email) & Q(is_superuser=True)).first():
            messages.error(request, 'User Not Found')
            return redirect('admin-forget-password')
        user_obj = User.objects.get(email=email)
        token = str(uuid.uuid4())
        user_obj.admin_forget_password_token = token
        user_obj.save()
        send_email_forget_pass(user_obj.email, token)
        messages.success(
            request, 'An email has been sent please check your email account')
        return redirect('admin-login')


def admin_change_password(request, token):
    if (request.method == 'GET'):
        obj = User.objects.get(admin_forget_password_token=token)
        Context = {"user_id": obj.id, }

        return render(request, 'loginUser/reset-password.html', Context)
    else:
        new_password = request.POST.get('new_pass')

        confirm_new_pass = request.POST.get('confirm_new_pass')
        user_id = User.objects.get(admin_forget_password_token=token)

        if not user_id:
            messages.error(request, 'User not found')
            return redirect('set-new-admin-password', user_id.admin_forget_password_token)
        if new_password != confirm_new_pass:
            messages.error(
                request, 'Password & confirm password does not match')
            return redirect('set-new-admin-password', user_id.admin_forget_password_token)
        user_obj = User.objects.get(id=user_id.id)
        user_obj.set_password(new_password)
        user_obj.save()
        messages.success(request, 'Your password successfully updated')
        return redirect('admin-login')

@login_required(login_url='admin-login')
def admin_logout(request):
    logout(request)
    return redirect('admin-login')


@login_required(login_url='admin-login')
def admindashboard(request):
    ctx={
            
        }
    alldata=User.objects.all()
    alluser=User.objects.all().count()
    ctx['alldata']=alldata
    ctx['alluser']=alluser
    ctx['a']="active"
    return render(request,'loginUser/index.html',context=ctx)




@login_required(login_url='admin-login')
def usermanagement(request):

    ctx={
            
        }
    alldata=User.objects.all()
    ctx['alldata']=alldata
    ctx['b']="active"
    
    return render(request,'loginUser/user-management.html',context=ctx)

@login_required(login_url='admin-login')
def profile(request):
     # print(request.user.is_authenticated)
    ctx={}

    if request.user.is_authenticated:
        user=User.objects.get(email=request.user.email)
        ctx['user']=user


    if request.method=='POST':
        # print(request.POST)
        firstName=request.POST.get('firstName','')
        email=request.POST.get('email','')
        image =request.FILES.get('image','')
        



        is_mail_exist=User.objects.filter(email=email).first()      

        
        if  firstName and email and ( request.user.email==email or not is_mail_exist):

            if image:
                user.firstName=firstName
                user.profileImage =  s3_helper(image)
                user.email=email 
                
                user.save()
            else:
                user.firstName=firstName
                user.email=email 
                
                user.save()
                print(user)

            
            # messages.success(request,'profile updated successfully')
            
            return redirect('profile')
          
    return render(request,'loginUser/profile.html',context=ctx)

def adduser(request):
    return render(request,"loginUser/add-user.html")



@login_required(login_url='admin-login')
def ChangePassword(request):
    ctx={

    }
    
    if request.user.is_authenticated:
        user=User.objects.get(email=request.user.email)
        ctx['user']=user
        
    if request.method=='POST' and request.POST.get('old-password','') and request.POST.get('password1','')  and request.POST.get('password2',''):
        old_password=request.POST.get('old-password','')
        password1=request.POST.get('password1','')  
        password2=request.POST.get('password2','')
        if password1 and password2 and password1==password2:
            
            if user.check_password(old_password):
                user.set_password(password1)
                user.save()
                return redirect('admin-login')

            

    return render(request,'loginUser/reset-password2.html',context=ctx)


# def filterdate(request):
#     if request.method == 'POST':
#         q = request.POST.get('filter_type_id2')
#         print('==',q)
#         if q == 'Today':
#             user = User.objects.filter(Q(createdAt__date=now()))
            

#         if q == 'Weekly':
#             current_date = datetime.date.today()
#             end_week = current_date - datetime.timedelta(7)
#             user = User.objects.filter(Q(createdAt__gte=end_week, createdAt__lte=current_date))
            

#         if q == 'Monthly':
#             current_date = datetime.date.today()
#             end_month = current_date - datetime.timedelta(30)
#             user = User.objects.filter(Q(createdAt__gte=end_month, createdAt__lte=current_date))

            

        
    #     context = {
    #         'alldata': user,
            
    #         'input': q,
            
    #         'rev': 'active'
    #     }

    # else:
    #     return redirect('user-management')
    # return render(request,'loginUser/user-management.html',context)
