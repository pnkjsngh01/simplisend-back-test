from django.contrib import admin
from django.urls import path
from userManagement import views
from userManagement.views import *


urlpatterns = [
     path('', views.admin_login, name='admin-login'),
     path('admin-logout', views.admin_logout, name='admin-logout'),
     path('admin-forget-password', views.admin_forget_password,name = "admin-forget-password"),
     path('admin-change-password/<token>/', views.admin_change_password, name='set-new-admin-password'),
     path('admin-dashboard', views.admindashboard, name='admin-dashboard'),
     path('user-management', views.usermanagement, name='user-management'),
     path('profile', views.profile, name='profile'),
     path('add-user', views.adduser, name='add-user'),
     path('change-password', views.ChangePassword, name='change-password'),
     # path('filter-date', views.filterdate, name='filter-date'),

]