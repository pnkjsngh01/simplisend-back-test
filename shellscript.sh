# we will check if virtualenv package is installed or not
virtualenv --version > /dev/null 2>&1
if [ $? == 0 ]
then
  echo "virtualenv is installed to create venv for python"
else
  echo $password | sudo -S apt install virtualenv -y
  echo "virtualenv has been installed"
fi

# Next we will check if venv is there or not
chmod -R 777 /var/www/html/simplisend-back-test/
cd /var/www/html/simplisend-back-test
ls | grep "simpli-env" > /dev/null 2>&1
if [ $? == 0 ]
then
  echo "Python vevn available"
  source simpli-env/bin/activate
else
  virtualenv -p python3 venv
  source simpli-env/bin/activate
  pip install -r requirements.txt
  echo "Python venv has been installed with requirements.txt"
fi


# Next we will migrate the changes
python3 manage.py makemigrations
python3 manage.py migrate


# Next we will start the server
nohup python manage.py runserver > /dev/null 2>&1 &
